package x5.demo.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class ControllerWithUserDetails {

    /**
     * get user details from context
     * @return user details
     */
    protected UserDetails getUserDetails(){
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * get username from user details
     * @return username
     */
    protected String getUserName(){
        return getUserDetails().getUsername();
    }

}
