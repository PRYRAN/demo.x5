package x5.demo.controller;

import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import x5.demo.dao.Item;
import x5.demo.dao.Order;
import x5.demo.service.OrderService;
import x5.demo.service.repository.ItemRepository;

@RestController
@RequestMapping("${api.path.order}")
@Log4j2
public class OrderController extends ControllerWithUserDetails{

    @Autowired
    OrderService orderService;

    @Autowired
    ItemRepository itemRepository;


    @GetMapping("/item")
    @PreAuthorize("hasRole('USER')")
    public Page<Item> getItems(Pageable pageable){
        log.info("Find all items request received");
        Page<Item> items = itemRepository.findAll(pageable);
        log.info("Find all items request processed, returning \n{}",items);
        return items;

    }

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public List<Order> getOrders(){
        log.info("Find user orders request received");
        List<Order> ordersForUser = orderService.getOrdersForUser(getUserName());
        log.info("Find user orders request processed, returning \n{}",ordersForUser);
        return ordersForUser;
    }

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public Order createOrder(@RequestParam(required = false) Set<Long> itemUids){
        log.info("Create order request received, with items {}",itemUids);
        Order order = orderService.createOrder(itemUids, getUserName());
        log.info("Order created, order \n{}", order);
        return order;
    }

    @DeleteMapping
    @PreAuthorize("hasRole('USER')")
    public void deleteOrder(@RequestParam Long uidOrder){
        log.info("Delete order request received, uid {}",uidOrder);
        orderService.deleteOrder(uidOrder,getUserName());
        log.info("Delete successful");
    }

}
