package x5.demo.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.ToString;

@Entity
@Table(name = "status")
@ToString
@Getter
public class Status {

    @Id
    @Column(name = "uid")
    private Long uid;

    @Column(name = "caption")
    private String caption;
}
