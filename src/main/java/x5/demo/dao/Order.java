package x5.demo.dao;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
import org.springframework.data.rest.core.annotation.RestResource;

@Entity
@Table(name = "ordering")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
public class Order {
    @Id
    @GeneratedValue
    @Column(name = "uid")
    private Long uid;

    @Column(name = "uid_user")
    private Long uidUser;

    @Column(name = "uid_status")
    @Setter
    private Long uidStatus;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "order_item",
            joinColumns = @JoinColumn(name = "uid_order"),
            inverseJoinColumns = @JoinColumn(name = "uid_item"))
    @RestResource(exported = false)
    @Exclude
    private Set<Item> items;

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
