package x5.demo.security.dto;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class AuthInfo {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

}
