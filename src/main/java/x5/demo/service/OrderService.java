package x5.demo.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import x5.demo.dao.Item;
import x5.demo.dao.Order;
import x5.demo.dao.Status;
import x5.demo.security.dao.User;
import x5.demo.security.service.UserDetailsServiceImpl;
import x5.demo.service.repository.ItemRepository;
import x5.demo.service.repository.OrderRepository;
import x5.demo.service.repository.StatusRepository;

@Service
@Log4j2
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    StatusRepository statusRepository;

    /**
     * @param userName username to check if user owns this order
     */
    public Order createOrder(Set<Long> itemUids, String userName) {

        Set<Item> items = new HashSet<>();
        if (itemUids != null && !itemUids.isEmpty()) {
            items = itemRepository.findAllByUidIn(itemUids);
            if (items.isEmpty()) {
                log.warn("no items for {} uids", itemUids);
            }
        }
        log.info("items for order to create {}", items);

        Status openStatus = statusRepository.getOpenStatus();

        User user = userDetailsService.getUser(userName);

        Order order = Order.builder()
                .uidUser(user.getUid())
                .uidStatus(openStatus.getUid())
                .items(items)
                .build();

        orderRepository.save(order);
        return order;
    }

    /**
     * delete(set status to closed) order by uid
     *
     * @param uidOrder uid
     * @param userName username to check if user owns this order
     */
    public void deleteOrder(Long uidOrder, String userName) {
        User user = userDetailsService.getUser(userName);

        Order order = orderRepository.findByUid(uidOrder);

        // if order is not found, then nothing to delete
        if (order == null || !order.getUidUser().equals(user.getUid())) {
            log.error("cannot delete order, no order for that uid {}", uidOrder);
            return;
        }

        Status status = statusRepository.getCloseStatus();

        order.setUidStatus(status.getUid());

        orderRepository.save(order);
        log.info("closed order is saved, {}", order);
    }

    /**
     * find orders that user owns
     *
     * @param userName username
     */
    public List<Order> getOrdersForUser(String userName) {
        User user = userDetailsService.getUser(userName);

        List<Order> orders = orderRepository.findAllByUidUser(user.getUid());

        return orders;

    }

}
