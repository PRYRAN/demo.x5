package x5.demo.service.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import x5.demo.dao.Order;


@RepositoryRestResource(collectionResourceRel = "order", path = "order")
public interface OrderRepository extends PagingAndSortingRepository<Order,Long> {

    @RestResource(exported = false)
    Order findByUid(Long uid);

    Page<Order> findAllByUidUser(Pageable pageable,Long uidUser);

    Page<Order> findAllByUidStatus(Pageable pageable, Long uidStatus);

    @RestResource(exported = false)
    List<Order> findAllByUidUser(Long uidUser);



}
