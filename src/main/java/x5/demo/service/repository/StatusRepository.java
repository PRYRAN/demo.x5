package x5.demo.service.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import x5.demo.dao.Status;

@RepositoryRestResource(collectionResourceRel = "status", path = "status")
public interface StatusRepository extends PagingAndSortingRepository<Status, Long> {

    @Query(value = "select * from status s where s.uid = 1", nativeQuery = true)
    Status getOpenStatus();

    @Query(value = "select * from status s where s.uid = 2", nativeQuery = true)
    Status getCloseStatus();

}
